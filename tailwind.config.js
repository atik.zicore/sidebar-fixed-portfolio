/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: "#29E98D",
        secondery: "#1F1E1E",
        customGray: "rgb(156 163 175 / 0.3)"
      },
    },
  },
  plugins: [],
}

