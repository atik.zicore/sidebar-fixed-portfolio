import logo from "./logo.svg";
import "./App.css";

import LeftSideMenu from "./Components/Home/LeftSideMenu/LeftSideMenu";
import Home from "./Components/Home";
import {
  LiaBarsSolid,
  LiaDribbble,
  LiaEnvelopeSolid,
  LiaGithub,
  LiaGripVerticalSolid,
  LiaHomeSolid,
  LiaInstagram,
  LiaShapesSolid,
  LiaStreamSolid,
  LiaTwitter,
  LiaUserSolid,
} from "react-icons/lia";
import {
  IoBriefcaseOutline,
  IoCloseOutline,
  IoHomeOutline,
} from "react-icons/io5";
import { useState } from "react";

function App() {
  const [menuBar, setMenuBar] = useState(false);

  return (
    <div className="App">
      <section className="relative bg-secondery w-full h-full">
        {/* Side Menu */}
       
          <div  className="flex justify-end sticky top-5 pe-[1.5%] z-50 h-full w-full">
            {menuBar ? (
              <IoCloseOutline
                className="w-8 h-8 text-white"
                onClick={() => setMenuBar(!menuBar)}
              />
            ) : (
              <LiaBarsSolid
                className="w-8 h-8 text-white"
                onClick={() => setMenuBar(!menuBar)}
              />
            )}
          </div>
          <div
           onClick={() => setMenuBar(false)}
            className={
              (menuBar
                ? "opacity-100 bg-black/40 h-screen w-full fixed inset-0 z-40 duration-100"
                : "pointer-events-none opacity-0") + " transition-all text-white"
            }
          >
           <div
           
              className={
                (menuBar ? "translate-x-0 " : "translate-x-full") + "w-96 bg-black absolute right-0 duration-1000  transition-all text-white"  
              }
            >
              <h2 className="pt-8">Menu</h2>
              <ul className="flex flex-col items-center pt-20  mx-auto w-60 h-screen  gap-y-8">
                <li className="flex  items-center w-full  gap-x-4 group cursor-pointer">
                  {" "}
                  <IoHomeOutline className="w-6 h-6 group-hover:text-primary" />
                  Home
                </li>
                <li className="flex items-center  w-full  gap-x-4 group cursor-pointer">
                  <LiaUserSolid className="w-6 h-6 group-hover:text-primary" />
                  User
                </li>
                <li className="flex items-center w-full  gap-x-4 group cursor-pointer">
                  <IoBriefcaseOutline className="w-6 h-6 group-hover:text-primary" />
                  Resume
                </li>
                <li className="flex items-center w-full  gap-x-4 group cursor-pointer">
                  <LiaStreamSolid className="w-6 h-6 group-hover:text-primary" />
                  Services
                </li>
                <li className="flex items-center w-full  gap-x-4 group cursor-pointer">
                  <LiaShapesSolid className="w-6 h-6 group-hover:text-primary" />
                  Skills
                </li>
                <li className="flex items-center w-full  gap-x-4 group cursor-pointer">
                  <LiaGripVerticalSolid className="w-6 h-6 group-hover:text-primary" />
                  Portfolio
                </li>
                <li className="flex items-center w-full  gap-x-4 group cursor-pointer">
                  <LiaEnvelopeSolid className="w-6 h-6 group-hover:text-primary" />
                  Contact
                </li>
              </ul>
            </div>
          </div>
          
      

        <section className="flex flex-col lg:flex-row w-full h-full">
          {/* Left Sidebar */}
          <div className="py-2 lg:pt-24 md:px-1 w-full lg:basis-[25%] h-full lg:sticky lg:top-[30%] lg:-translate-y-[30%]  ">
            <LeftSideMenu />
          </div>
          {/* Main Content */}
          <div className="lg:basis-[60%] flex-1 text-white py-4 px-4 lg:ps h-full w-full  bg-green-400">
            <Home />
          </div>
          {/* Right Sidebar */}
          <div className="lg:basis-[5%] hidden lg:block lg:sticky lg:top-[40%] lg:-translate-y-[40%] lg:right-0 h-full ">
            <ul className="gap-y-4 flex flex-col justify-center items-center w-max mx-auto px-4 py-8 rounded-full border border-gray-600">
              <li>
                <LiaInstagram className="h-8 w-8 text-gray-400" />
              </li>
              <li>
                <LiaTwitter className="h-7 w-7 text-gray-400" />
              </li>
              <li>
                <LiaDribbble className="h-7 w-7 text-gray-400" />
              </li>
              <li>
                <LiaGithub className="h-7 w-7 text-gray-400" />
              </li>
              <li>
                <LiaEnvelopeSolid className="h-7 w-7 text-gray-400" />
              </li>
            </ul>
          </div>
        </section>
      </section>
    </div>
  );
}

export default App;
