import React from "react";
import {
  LiaDribbble,
  LiaEnvelope,
  LiaGithub,
  LiaInstagram,
  LiaTwitter,
} from "react-icons/lia";

const LeftSideMenu = () => {
  return (
    <div>
      <div className=" px-1 bg-secondery border border-customGray rounded-lg ">
        {/* Grid Container */}
        <div className="px-4">
          <div className="flex items-center justify-between text-white">
            <img
              src="/Images/logo.png"
              alt="logo"
              className="w-24 h-14 object-contain"
            />
            <p className="text-sm">
              Framer Designer <br />& Developer
            </p>
          </div>
          <div className="max:w-50 max:h-50 mx-auto py-4">
            <img
              src="/Images/me.jpg"
              alt="Profile Image"
              className="object-contain w-30 h-30 mx-auto rounded-xl"
            />
          </div>
          <div>
            <div className="text-white">
              <h4 className="text-lg"> Hello@Drake.design</h4>
              <h4 className="text-lg">Based in Los Angeles, CA</h4>
              <p className="capitalize text-customGray pt-6">
                @2024 Drake. All right reserved
              </p>
            </div>
          </div>
          {/* Social Icon */}
          <div className="flex items-center justify-center py-4 gap-x-4 flex-wrap">
            <a
              href="https://www.youtube.com/watch?v=oP5yKkKxLvE"
              target="_blank"
              className="border-2 rounded-full border-gray-600/50"
            >
              <LiaInstagram className="w-10 h-10 sm:w-12 sm:h-12  p-3 sm:p-4 text-customGray" />
            </a>
            <a href="" className="border-2 border-gray-600/50 rounded-full">
              <LiaTwitter className="w-10 h-10 sm:w-12 sm:h-12 text-customGray p-3 sm:p-4" />
            </a>
            <a
              href=""
              type="button"
              className="border-2 border-gray-600/50 rounded-full"
            >
              <LiaDribbble className="w-10 h-10 sm:w-12 sm:h-12 text-customGray p-3 sm:p-4" />
            </a>
            <a
              href=""
              type="button"
              className="border-2 border-gray-600/50 rounded-full"
            >
              <LiaGithub className="w-10 h-10 sm:w-12 sm:h-12 text-customGray p-3 sm:p-4" />
            </a>
          </div>
          {/* Hire Me Button */}
          <div className="pb-6 pt-2">
            <div className="flex justify-center items-center bg-primary rounded-full py-2">
              <button className="flex items-center gap-x-4 ">
                <LiaEnvelope className="w-6 h-6 " />{" "}
                <span className="text-base">Hire me</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LeftSideMenu;
